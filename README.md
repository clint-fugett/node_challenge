# Overview
This is a simple [CRUD](https://www.codecademy.com/articles/what-is-crud) application demonstating the [MEAN](https://www.mongodb.com/mean-stack) application programming stack.  The application provides a simple user interface allowing CRUD operation on "Products" with the following requirements:

1. The application must be implemented using a RESTful service for the backend, written in Node.js version 10 or higher.  You may use TypeScript.
2. The front-end must be implemented using Angular version 4 or higher.
3. Each product shall have the following attributes
-- name (must be a string between 3 and 100 characters and must be unique in the system)
-- description (must be a string between 5 and 1000 characters)
-- price (must be a number between 1 and 20,000 with 2 decimal place precision)
4. The application shall allow the user to create new products
5. The application shall allow the user to list all of the products in the system
6. The application shall allow the user to retrieve the details of any product by its name
7. The application shall allow the user to update a product’s description or price
8. The application shall allow the user to delete a product

# Running the application in Docker
##### 1. [Install Docker](https://docs.docker.com/get-docker/)
##### 2. Execute the "run.sh" bash script
##### 3. Execute the "cleanup.sh" bash script when you're finished.
##### 4. Navigate to http://localhost:4200/ in ya browsa!

# Running locally for development
##### 1. Install Prerequisite utilities to run the application:
- [Docker](https://docs.docker.com/get-docker/)
- [Node](https://nodejs.org/en/download/) version v14.17.4 or greater
##### 2. Install MongoDB or run it via Docker.
- To run via Docker, use the following Docker command ```docker run -it -d -p 27017:27017 --name mongo mongo```
##### 3. Run the backend development server
- From the node_challenge/backend directory run ```npm install```
- From the node_challenge/backend directory run ```npm run server```
##### 4. Run the frontend development server
- From the node_challenge/frontend directory run ```npm install```
- From the node_challenge/frontend directory run ```ng serve```
##### 5. Navigate to http://localhost in ya browsa!

# Running Tests
- To run the frontend tests, execute the following from the node_challenge/frontend directory: ```ng test```
- To run the backend tests, execute the following from the node_challenge/backend directory: ```ng test```