const assert = require('assert');
const app = require('../src/app.js');
const Product = require('../src/product.js');

//Product Creation tests
describe('When successfully creating a new product', () => {
    it('it should return a 201 status code', () => {
        
    });

    it('it should return the id of the newly created product', () => {
        //612c06fc1c5035711de74ded
    });
});

describe('When creating a new product with an empty name', () => {
    it('it should return a 400 status code', () => {
        
    });

    it('it should return an error message', () => {
        //Product Name must be between 3 and 100 characters!
    });
});

describe('When creating a new product with the name being too long', () => {
    it('it should return 400 status code', () => {
        
    });

    it('it should return an error message', () => {
       //Product Name must be between 3 and 100 characters! 
    });
});

describe('When creating a new product with a name that already exists', () => {
    it('it should return 400 status code', () => {
        
    });

    it('it should return an error message', () => {
        //Product Name must be unique in the system
    });
});

describe('When creating a new product with an empty description', () => {
    it('it should return 400 status code', () => {
        
    });

    it('it should return error message', () => {
        //Product Description must be between 5 and 1000 characters!
    });
});

describe('When creating a new product with the description being too long', () => {
    it('it should return 400 status code', () => {
        
    });

    it('it should return error message', () => {
        //Product Description must be between 5 and 1000 characters!
    });
});

describe('When creating a new product with an empty price', () => {
    it('it should return 400 status code', () => {
        
    });

    it('it should return error message', () => {
        //Product Price must be in the format $XXXXX.XX (no commas)
    });
});

describe('When creating a new product with the price too low', () => {
    it('it should return 400 status code', () => {
        
    });

    it('it should return error message', () => {
        //Product Price must be a dollar amount between $1.00 and $20,000.00!
    });
});

describe('When creating a new product with the price too high', () => {
    it('it should return 400 status code', () => {
        
    });

    it('it should return error message', () => {
        //Product Price must be a dollar amount between $1.00 and $20,000.00!
    });
});

describe('When creating a new product with an invalid price', () => {
    it('it should return 400 status code', () => {
        
    });

    it('it should return error message', () => {
        //Product Price must be a dollar amount between $1.00 and $20,000.00!
    });
});


// Product update tests
describe('When successfully updating an existing product', () => {
    it('it should return a 200 status code', () => {
        
    });

    it('it should return the id of the updated product', () => {
        //612c06fc1c5035711de74ded
    });
});

describe('When updating an existing product with an empty name', () => {
    it('it should return a 400 status code', () => {
        
    });
    
    it('it should return an error message', () => {
        //Product Name must be between 3 and 100 characters!
    });
});

describe('When updating an existing product with the name being too long', () => {
    it('it should return 400 status code', () => {
        
    });

    it('it should return an error message', () => {
       //Product Name must be between 3 and 100 characters! 
    });
});

describe('When updating an existing product with a name that already exists', () => {
    it('it should return 400 status code', () => {
        
    });

    it('it should return an error message', () => {
        //Product Name must be unique in the system
    });
});

describe('When updating an existing product with an empty description', () => {
    it('it should return 400 status code', () => {
        
    });

    it('it should return error message', () => {
        //Product Description must be between 5 and 1000 characters!
    });
});

describe('When updating an existing product with the description being too long', () => {
    it('it should return 400 status code', () => {
        
    });

    it('it should return error message', () => {
        //Product Description must be between 5 and 1000 characters!
    });
});

describe('When updating an existing product with an empty price', () => {
    it('it should return 400 status code', () => {
        
    });

    it('it should return error message', () => {
        //Product Price must be in the format $XXXXX.XX (no commas)
    });
});

describe('When updating an existing product with the price too low', () => {
    it('it should return 400 status code', () => {
        
    });

    it('it should return error message', () => {
        //Product Price must be a dollar amount between $1.00 and $20,000.00!
    });
});

describe('When updating an existing product with the price too high', () => {
    it('it should return 400 status code', () => {
        
    });

    it('it should return error message', () => {
        //Product Price must be a dollar amount between $1.00 and $20,000.00!
    });
});

describe('When updating an existing product with an invalid price', () => {
    it('it should return 400 status code', () => {
        
    });

    it('it should return error message', () => {
        //Product Price must be a dollar amount between $1.00 and $20,000.00!
    });
});

// Product deletion tests
describe('When deleting a product that exists in the system', () => {
    it('it should return a 400 status code', () => {

    });

    it('it should return the id of the deleted product', () => {
        //612c06fc1c5035711de74ded
    });
});

// Product fetch tests
describe('When a GET request is performed against the /api/products endpoint', () => {
    it('it should return a 200 status code', () => {

    });

    it('it should return a json list of product objects', () => {
        // [
        //     {
        //         "_id":"612c06fc1c5035711de74ded",
        //         "name":"Product #1",
        //         "description":"Product #1 Description",
        //         "price":"$1.00",
        //         "__v":0
        //     },
        //     {
        //         "_id":"612c07051c5035711de74df1",
        //         "name":"Product #2",
        //         "description":"Product #2 Description",
        //         "price":"$2.00",
        //         "__v":0
        //     },
        //     {
        //         "_id":"612c07a91c5035711de74e0c",
        //         "name":"Product #3",
        //         "description":"Product #3 Description",
        //         "price":"$3.00",
        //         "__v":0
        //     }
        // ]
    });
});