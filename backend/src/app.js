const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const Product = require('./product.js');
const mongoose = require('mongoose');
const app = express();

app.use(cors());
app.options('*', cors());
app.use(bodyParser.json());

//Running in Docker
mongoose.connect('mongodb://host.docker.internal:27017/products', {useNewUrlParser: true}).then(() => {
    console.log('Connected to database!');
})
.catch(() => {
    //Running locally using dev servers
    mongoose.connect('mongodb://localhost:27017/products', {useNewUrlParser: true}).then(() => {
        console.log('Connected to database!');
    })
    .catch(() => {
        console.log('Connection failed!');
    }); 
});

app.post('/api/products', (req, res) => {
    validateAndCreateProduct(false, req, res).then((product) => {
        if(product !== null) {
            product.save().then((result) => {
                res.status(201).json({
                    "_id": result._id
                });
            });
        }
    }).catch((errorMessages) => {
        res.status(400).json({
            "errorMessages": errorMessages
        });
    });
});

app.put('/api/products/:_id', (req, res) => {
    const _id = req.params._id;
    validateAndUpdateProduct(true, req).then((updatedProduct) => {
        Product.findByIdAndUpdate(_id, updatedProduct).then(() => {
            res.status(200).json({
                "_id": _id
            });
        });
    }).catch((errorMessages) => {
        res.status(400).json({
            "errorMessages": errorMessages
        });
    });
});

app.delete('/api/products/:_id', (req, res) => {
    const _id = req.params._id;
    const product = Product.findOneAndDelete({_id: _id}, {
    }, () => {
        res.status(200).json({
            "_id": _id
        });
    });
});

app.get('/api/products', (req, res) => {
    Product.find().then((products) => {
        res.status(200).json(products);
    });
});

async function validateAndUpdateProduct(editing, req) {
    const errorMessages = [];

    const {name, description, price } = req.body;

    await validateNameUniqueInSystem(editing, name, errorMessages);
    validateName(name, errorMessages);
    validateDescription(description, errorMessages);
    validatePrice(price, errorMessages);

    if(errorMessages.length > 0) {
        return Promise.reject(errorMessages);
    }

    return Promise.resolve({
        name: name,
        description: description,
        price: price
    });
}

async function validateAndCreateProduct(editing, req) {
    const errorMessages = [];

    const { name, description, price } = req.body;

    await validateNameUniqueInSystem(editing, name, errorMessages);
    validateName(name, errorMessages);
    validateDescription(description, errorMessages);
    validatePrice(price, errorMessages);

    if(errorMessages.length > 0) {
        return Promise.reject(errorMessages);
    }
    
    return new Product({
        name: name,
        description: description,
        price: price
    });
}

function validateName(name, errorMessages) {
    if(name.length < 3 || name.length > 100) {
        errorMessages.push("Product Name must be between 3 and 100 characters!");
    }
}

async function validateNameUniqueInSystem(editing, name, errorMessages) {
    if(!editing) {
        const product = await Product.findOne({ name: name });
        if(product !== null) {
            errorMessages.push("Product Name must be unique in the system");
        }
    }
}

function validateDescription(desc, errorMessages) {
    if(desc.length < 5 || desc.length > 1000) {
        errorMessages.push("Product Description must be between 5 and 1000 characters!");
    }
}

function validatePrice(price, errorMessages) {
    if(!new RegExp('^\\$\\d*\\.\\d{2}$').test(price)) {
        errorMessages.push("Product Price must be in the format $XXXXX.XX (no commas)");
        return;
    }
    if(price.includes(',')) {
        price = price.replace(',','');
    }
    if(price.includes('$')) {
        price = price.replace('$', '');
    }
    let priceAsNumber = Number(price);
    if(isNaN(priceAsNumber) || priceAsNumber < 1 || priceAsNumber > 20000) {
        errorMessages.push("Product Price must be a dollar amount between $1.00 and $20,000.00!")
    }
}

module.exports = app;