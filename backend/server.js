const app = require('./src/app.js')
const http = require('http');

const port = "3000";

app.set("port", port);

const server = http.createServer(app);
server.on("listening", () => {
    const addr = server.address();
    const bind = typeof addr === "string" ? "pipe " + addr : "port " + port;
    console.log("Listening on " + bind);
});
server.listen(port);