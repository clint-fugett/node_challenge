import { Injectable } from '@angular/core';
import { Product } from '../../../models/Product'
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  products:BehaviorSubject<Map<string, Product>> = new BehaviorSubject(new Map());
  currentProduct:BehaviorSubject<Product> = new BehaviorSubject(new Product());
  editing:BehaviorSubject<boolean> = new BehaviorSubject(Boolean(false));
  readonly:BehaviorSubject<boolean> = new BehaviorSubject(Boolean(false));

  constructor(private http: HttpClient) {}

  save(product:Product, errorMessages:Array<string>) {
    this.http.post<any>('http://localhost:3000/api/products', product).subscribe(
      data => {
        console.log(`Successfully created product with id ${data._id}`);
        this.getProducts();
        errorMessages = new Array<string>();
      },
      error => {
        const serverErrors = error.error.errorMessages;
        for (var errorIndex in serverErrors) {
          errorMessages.push(serverErrors[errorIndex]);
        }
      });
  }

  update(product:Product, errorMessages:Array<string>) {
    const url = `http://localhost:3000/api/products/${product._id}`;
    this.http.put<{_id:string}>(url, product).subscribe(
      data => {
        console.log(`Successfully updated product with id ${product._id}`);
        this.getProducts();
        errorMessages = new Array<string>();
      },
      error => {
        const serverErrors = error.error.errorMessages;
        for (var errorIndex in serverErrors) {
          errorMessages.push(serverErrors[errorIndex]);
        }
      });
  }

  delete(product:Product) {
    const url = `http://localhost:3000/api/products/${product._id}`;
    this.http.delete(url).subscribe(() => {
      console.log(`Successfully deleted product with id ${product._id}`);
      this.getProducts();
    });
  }

  getProducts() {
    this.http.get('http://localhost:3000/api/products').subscribe((data) => {
      const products:Map<string, Product> = new Map();
      for (const [key, value] of Object.entries(data)) {
        products.set(key, value);
      }
      this.products.next(products);
    });
    return this.products;
  }

}
