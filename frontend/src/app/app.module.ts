import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';


import { AppComponent } from './components/app.component';
import { HeaderComponent } from './components/shared/header/header.component';
import { ProductEditorComponent } from './components/product-editor/product-editor.component';
import { ProductFormComponent } from './components/product-editor/product-form/product-form.component';
import { ProductSummaryComponent } from './components/product-editor/product-list/product-summary/product-summary.component';
import { ProductListComponent } from './components/product-editor/product-list/product-list.component';
import { ProductSummaryHeaderComponent } from './components/product-editor/product-list/product-summary/product-summary-header/product-summary-header.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ProductEditorComponent,
    ProductFormComponent,
    ProductSummaryComponent,
    ProductListComponent,
    ProductSummaryHeaderComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
