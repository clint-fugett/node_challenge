import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product/product.service';
import { Product } from '../../../../models/Product';

@Component({
  selector: 'product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  constructor(private productService: ProductService) { }

  products:Array<Product> = [];

  ngOnInit() {
    this.productService.products.subscribe(products => {
        this.products = Array.from(products.values());
    });
  }
}
