import { Component, Input } from '@angular/core';
import { Product } from '../../../../../models/Product';

@Component({
  selector: 'product-summary',
  templateUrl: './product-summary.component.html',
  styleUrls: ['./product-summary.component.css']
})
export class ProductSummaryComponent {
  constructor() {}

  @Input() product:Product = new Product();

}
