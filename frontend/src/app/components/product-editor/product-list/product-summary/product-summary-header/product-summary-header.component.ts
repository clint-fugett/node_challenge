import { Component, Input } from '@angular/core';
import { ProductService } from 'src/app/services/product/product.service';
import { Product } from '../../../../../../models/Product';

@Component({
  selector: 'product-summary-header',
  templateUrl: './product-summary-header.component.html',
  styleUrls: ['./product-summary-header.component.css']
})
export class ProductSummaryHeaderComponent {

  constructor(private productService:ProductService) {}

  @Input() color:string = 'primary';
  @Input() title:string = '';
  @Input() product:Product = new Product("", "", "");

  viewProduct() {
    this.productService.currentProduct.next(this.product);
    this.productService.editing.next(false);
    this.productService.readonly.next(true);
  }

  editProduct() {
    this.productService.currentProduct.next(this.product);
    this.productService.editing.next(true);
    this.productService.readonly.next(false);
  }

  deleteProduct() {
    this.productService.delete(this.product);
    this.productService.currentProduct.next(new Product());
    this.productService.editing.next(false);
    this.productService.readonly.next(false);
  }
}
