import { Component, OnInit} from '@angular/core';
import { ProductService } from 'src/app/services/product/product.service';
import { Product } from '../../../../models/Product'

@Component({
  selector: 'product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {

  constructor(private productService:ProductService) {}

  currentProduct:Product = new Product();
  errorMessages:Array<string> = [];
  editing:boolean = false;
  readonly:boolean = false;

  ngOnInit() {
      this.productService.getProducts();
      this.productService.currentProduct.subscribe(currentProduct => {
          this.currentProduct = currentProduct;
      });
      this.productService.editing.subscribe(editing => {
          this.editing = editing;
      });
      this.productService.readonly.subscribe(readonly => {
        this.readonly = readonly;
      });
  }

  saveProduct(productName: HTMLInputElement, productDescription: HTMLTextAreaElement, productPrice: HTMLInputElement) {
    this.errorMessages = [];
    this.currentProduct = new Product(productName.value, productDescription.value, productPrice.value);
    this.productService.save(this.currentProduct, this.errorMessages);
  }

  updateProduct(productId: HTMLInputElement, productName: HTMLInputElement, productDescription: HTMLTextAreaElement, productPrice: HTMLInputElement) {
    this.errorMessages = [];
    this.currentProduct = new Product(productName.value.trim(), productDescription.value.trim(), productPrice.value.trim());
    this.currentProduct._id = productId.value;
    this.productService.update(this.currentProduct, this.errorMessages);
  }

  closeReadonly() {
    this.productService.readonly.next(Boolean(false));
    this.productService.editing.next(Boolean(false));
    this.resetForm();
  }

  resetForm() {
    this.productService.currentProduct.next(new Product());
    this.productService.editing.next(Boolean(false));
    this.productService.readonly.next(Boolean(false));
    this.errorMessages = [];
  }

}
