export class Product {
  _id:string;
  name: string;
  description: string;
  price: string;

  constructor(name?:string, description?:string, price?:string) {
    this._id = undefined!;
    this.name = name || '';
    this.description = description || '';
    this.price = price || '';
  }

  getShortName() {
    if(this.name != undefined && this.name != '' && this.name.length >= 20) {
      return this.name.substr(0, 20) + '...';
    } else if (name != undefined) {
      return this.name
    }
    return '';
  }

  getShortDescription() {
    if(this.description != undefined && this.description != '' && this.description.length >= 200) {
      return this.description.substr(0, 200) + '...';
    } else if (this.description != undefined) {
      return this.description;
    }
    return '';
  }

}
