#!/bin/bash

docker container kill mongo
docker container rm mongo
docker image rm mongo
docker container kill frontend
docker container rm frontend
docker image rm frontend
docker container kill backend
docker container rm backend
docker image rm backend