#!/bin/bash

docker container kill mongo
docker container rm mongo
docker run -it -d -p 27017:27017 --name mongo mongo

cd backend
docker container kill backend
docker container rm backend
docker build . -t backend
docker run -it -d -p 3000:3000 --name backend backend

cd ../frontend
docker container kill frontend
docker container rm frontend
docker build . -t frontend
docker run -it -d -p 80:80 --name frontend frontend
